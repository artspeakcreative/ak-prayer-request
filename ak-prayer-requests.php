<?php
/*
Plugin Name: ArtSpeak Prayer Requests
Plugin URI: http://artspeakcretive.com
Description: Prayer request sharing functionality for ArtSpeak sites
Version: 0.1
Author: Drew Sartorius
Author URI: http://artspeakcreative.com
License: GPL2
 */

//  PLUGIN FUNCTIONS
function ak_prayer_activation()
{
 // Run when plugin is activated
}
register_activation_hook(__FILE__, 'ak_prayer_activation');

function ak_prayer_deactivation()
{
 // Run when plugin is deactivated
}
register_deactivation_hook(__FILE__, 'ak_prayer_deactivation');

function ak_prayer_uninstall()
{
 // Run when plugin is uninstalled
}
register_uninstall_hook(__FILE__, 'ak_prayer_uninstall');

// HOOKS FUNCTIONS
function ak_prayer_register_post_types()
{
 include "PostTypes/Prayer.php";
}
add_action('after_theme_register_post_types', 'ak_prayer_register_post_types');

function ak_prayer_add_to_context($context)
{
 $context['prayers'] = Timber::get_posts(['post_type' => 'prayer', 'number_posts' => -1]);

 return $context;
}
add_filter('akmt_add_to_context', 'ak_prayer_add_to_context');

function ak_prayer_add_to_twig($twig)
{
 //  PRAYER FUNCTIONS
 $prayers = new Twig_SimpleFunction('prayers', function () {
  $args = array(
   'post_type' => 'prayer',
   'numberposts' => 24,
   'orderby' => 'date',
   'order' => 'DESC',
   'meta_query' => array(
    array(
     'key' => 'publishable',
     'value' => 1,
     'compare' => 'LIKE',
    ),
   ),
  );

  $prayers = Timber::get_posts($args);
  error_log("Prayers: " . print_r($prayers, true));

  return $prayers;
 });
 $twig->addFunction($prayers);

 return $twig;
}
add_filter('akmt_add_to_twig', 'ak_prayer_add_to_twig');

// TEMPLATE OVERRIDE
add_filter('template_include', 'ak_prayer_override_templates');
function ak_prayer_override_templates($template)
{
 if (is_post_type_archive('prayer')) {
  $theme_files = array('archive-prayer.php', 'ak-prayer-requests/archive-prayer.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__)) . '/archive-prayer.php';
  }
 }
 return $template;

}
