<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$templates = array('archive.twig', 'index.twig');

$context = Timber::get_context();

$post_type = get_queried_object()->name;

$context['title'] = 'Archive';
if (is_day()) {
 $context['title'] = 'Archive: ' . get_the_date('D M Y');
} else if (is_month()) {
 $context['title'] = 'Archive: ' . get_the_date('M Y');
} else if (is_year()) {
 $context['title'] = 'Archive: ' . get_the_date('Y');
} else if (is_tag()) {
 $context['title'] = single_tag_title('', false);
 $context['filters'] = get_filters($post_type);
} else if (is_category()) {
 $context['title'] = single_cat_title('', false);
 $context['filters'] = get_filters($post_type);
 array_unshift($templates, 'archive-' . get_query_var('cat') . '.twig');
} else if (is_post_type_archive()) {
 $context['title'] = post_type_archive_title('', false);
 $context['filters'] = get_filters($post_type);
 array_unshift($templates, 'archive-' . $post_type . '.twig');
} else if (is_tax()) {
 $context['title'] = single_term_title('', false);
 $context['filters'] = get_filters($post_type);
 array_unshift($templates, 'archive-' . $post_type . '.twig');
 array_unshift($templates, 'archive-' . $post_type . 's.twig');
}

$args = array(
 'post_type' => 'prayer',
 'orderby' => 'date',
 'order' => 'DESC',
 'meta_query' => array(
  array(
   'key' => 'publishable',
   'value' => 1,
   'compare' => 'LIKE',
  ),
 ),
);

$context['posts'] = Timber::get_posts($args);
$context['pagination'] = Timber::get_pagination();
//$context['foo'] = get_field('image', get_term(1) );
if ($context['options']['transient_refresh'] > 0) {
 Timber::render($templates, $context, $context['options']['transient_refresh']);
} else {
 Timber::render($templates, $context);
}
