<?php
namespace PostType;

// include get_template_directory() . "/library/PostType/BasePost.php";

/*
 * Community Event Post Type
 */
class Prayer extends BasePost
{
 public static $form_id;
 const POST_TYPE = "Prayer";

 /**
  * CONSTRUCTOR - - - - - - - - - - - - - - - - - - -
  */
 public function __construct($post)
 {
  parent::__construct($post);

  $this->expire();
 }

 /**
  * STATIC FUNCTIONS - - - - - - - - - - - - - - - -
  */
 public static function setup()
 {
  Prayer::register();
  Prayer::fields();
  self::$form_id = Prayer::create_form();
  add_shortcode('prayer-form', array('PostType\Prayer', 'form_shortcode'));
 }

 public static function register()
 {
  register_extended_post_type(self::POST_TYPE, array(
   'show_in_feed' => false,
   'menu_icon' => 'dashicons-buddicons-pm',
   'enter_title_here' => 'Prayer Request Title',
   'admin_cols' => array(
    'featured_image' => array(
     'title' => 'Image',
    ),
    'title',
    'date',
   )), array(

   # Override the base names used for labels:
   'singular' => 'Prayer Request',
   'plural' => 'Prayer Requests',
   'slug' => 'prayer',
  ));

  register_extended_taxonomy(
   'prayer_category',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Prayer Category',
    'plural' => 'Prayer Categories',
    'slug' => 'prayer_category',
   ));
 }

 public static function fields()
 {
  $group_key = str_replace(' ', '_', self::POST_TYPE) . '_group';
  $group = array(
   /* (string) Unique identifier for field group. Must begin with 'group_' */
   'key' => $group_key,

   /* (string) Visible in metabox handle */
   'title' => self::POST_TYPE . " Settings",

   /* (array) An array of fields */
   'fields' => array(),

   /* (array) An array containing 'rule groups' where each 'rule group' is an array containing 'rules'.
   Each group is considered an 'or', and each rule is considered an 'and'. */
   'location' => array(
    array(
     array(
      'param' => 'post_type',
      'operator' => '==',
      'value' => strtolower(self::POST_TYPE),
     ),
    ),
   ),

   /* (int) Field groups are shown in order from lowest to highest. Defaults to 0 */
   'menu_order' => 0,

   /* (string) Determines the position on the edit screen. Defaults to normal. Choices of 'acf_after_title', 'normal' or 'side' */
   'position' => 'normal',

   /* (string) Determines the metabox style. Defaults to 'default'. Choices of 'default' or 'seamless' */
   'style' => 'default',

   /* (string) Determines where field labels are places in relation to fields. Defaults to 'top'.
   Choices of 'top' (Above fields) or 'left' (Beside fields) */
   'label_placement' => 'top',

   /* (string) Determines where field instructions are places in relation to fields. Defaults to 'label'.
   Choices of 'label' (Below labels) or 'field' (Below fields) */
   'instruction_placement' => 'label',

   /* (array) An array of elements to hide on the screen */
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_name",
   'label' => 'Name',
   'name' => 'name',
   'instructions' => 'This name will not get displayed',
   'type' => 'text',
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_email",
   'label' => 'Email',
   'name' => 'email',
   'type' => 'text',
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_phone",
   'label' => 'Phone Number',
   'name' => 'phone',
   'type' => 'text',
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_city",
   'label' => 'City',
   'name' => 'city',
   'type' => 'text',
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_prayer_for",
   'label' => 'Person to Pray For',
   'name' => 'prayer_for',
   'instructions' => 'The name that will get publically displayed.',
   'type' => 'text',
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_update",
   'label' => 'Is this an update',
   'name' => 'update',
   'type' => 'radio',
   'choices' => array(
    'yes' => 'Yes',
    'no' => 'No',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_publishable",
   'label' => '',
   'name' => 'publishable',
   'type' => 'checkbox',
   'choices' => array(1 => 'Can display online'),
   'parent' => $group_key,
  ));
 }

 /**
  * Create the Community Events Form
  */
 public static function create_form()
 {
  if (!class_exists("GFAPI")) {
   return;
  }
  $form_name = "Prayer Request Form";

  // Check to see if the form already exists
  $form_id = \RGFormsModel::get_form_id($form_name);
  if ($form_id) {
   return $form_id;
  }

  $json = '{"0":{"title":"Prayer Request Form","description":"","labelPlacement":"top_label","descriptionPlacement":"below","button":{"type":"text","text":"Submit","imageUrl":""},"fields":[{"type":"post_custom_field","id":10,"label":"Your Name","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"inputType":"text","formId":5,"description":"(confidential - not posted online)","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"name","displayOnly":""},{"type":"post_custom_field","id":11,"label":"Email","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"inputType":"email","formId":5,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"email","displayOnly":""},{"type":"post_custom_field","id":12,"label":"Phone","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"inputType":"text","formId":5,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"phone","displayOnly":""},{"type":"post_custom_field","id":13,"label":"City","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"inputType":"text","formId":5,"description":"(not posted online - used to provide local resources to you)","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"city","displayOnly":""},{"type":"post_title","id":9,"label":"Post Title","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"formId":5,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"hide","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"{date_mdy}","choices":"","conditionalLogic":"","failed_validation":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"saveAsCPT":"prayer","displayOnly":""},{"type":"post_custom_field","id":18,"label":"Person to Pray For","adminLabel":"","isRequired":true,"size":"medium","errorMessage":"","inputs":null,"inputType":"text","formId":5,"description":"(This will be posted online if you check the Post this request online checkbox)","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"prayer_for"},{"type":"post_custom_field","id":14,"label":"Is this an update?","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":null,"inputType":"radio","formId":5,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":[{"text":"Yes","value":"yes","isSelected":false,"price":""},{"text":"No","value":"no","isSelected":false,"price":""}],"conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"postCustomFieldName":"update","enableChoiceValue":true,"displayOnly":""},{"type":"post_content","id":16,"label":"Prayer","adminLabel":"","isRequired":true,"size":"medium","errorMessage":"","inputs":null,"formId":5,"description":"(max 500 chars)","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","inputType":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":"","conditionalLogic":"","failed_validation":"","productField":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"maxLength":"500","displayOnly":""},{"type":"post_custom_field","id":17,"label":"","adminLabel":"","isRequired":false,"size":"medium","errorMessage":"","inputs":[{"id":"17.1","label":"Post this request online","name":""}],"inputType":"checkbox","formId":5,"description":"","allowsPrepopulate":false,"inputMask":false,"inputMaskValue":"","labelPlacement":"","descriptionPlacement":"","subLabelPlacement":"","placeholder":"","cssClass":"","inputName":"","adminOnly":false,"noDuplicates":false,"defaultValue":"","choices":[{"text":"Post this request online","value":"1","isSelected":false,"price":""}],"conditionalLogic":"","failed_validation":"","productField":"","enablePasswordInput":"","maxLength":"","multipleFiles":false,"maxFiles":"","calculationFormula":"","calculationRounding":"","enableCalculation":"","disableQuantity":false,"displayAllCategories":false,"useRichTextEditor":false,"enableChoiceValue":true,"postCustomFieldName":"publishable","displayOnly":""}],"version":"2.0.7","id":5,"useCurrentUserAsAuthor":true,"postContentTemplateEnabled":false,"postTitleTemplateEnabled":false,"postTitleTemplate":"","postContentTemplate":"","lastPageButton":null,"pagination":null,"firstPageCssClass":null,"postAuthor":"1","postCategory":"1","postFormat":"0","postStatus":"draft","confirmations":[{"id":"5e74c8b2b28c4","name":"Default Confirmation","isDefault":true,"type":"message","message":"Thanks for contacting us! We will get in touch with you shortly.","url":"","pageId":"","queryString":""}],"notifications":[{"id":"5e74c8b2b252e","to":"{admin_email}","name":"Admin Notification","event":"form_submission","toType":"email","subject":"New submission from {form_title}","message":"{all_fields}"}]},"version":"2.0.7"}';

  $form = json_decode($json, true);
  $result = \GFAPI::add_form($form[0]);

  return $result;
 }

 /**
  * Form Shortcode
  */
 public static function form_shortcode($atts, $content = null)
 {
  global $post;

  $defaults = array(
   'name' => '',
  );
  extract(shortcode_atts($defaults, $atts));

  $form_id = self::$form_id;

  $gform_shortcode = "[gravityform id='{$form_id}' title='false' description='false']";

  return do_shortcode($gform_shortcode);
 }

 /**
  * HELPER FUNCTIONS - - - - - - - - - - - - - - - -
  */
 public function expire()
 {
  $now = current_datetime();
  $now->add(new \DateInterval('P30D'));
  $post_date = new \DateTimeImmutable($this->post_date, wp_timezone());
  $expired = $post_date > $now;

  if (strlen($this->prayer_for) > 0 && $this->publishable == 0) {
   wp_delete_post($this->ID);
  }

  if ($expired) {
   $current_post = get_post($this->ID, 'ARRAY_A');
   $current_post['post_status'] = 'draft';
   wp_update_post($current_post);
  }
 }
}

Prayer::setup();
